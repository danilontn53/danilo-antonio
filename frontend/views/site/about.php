<?php

/* @var $this yii\web\View */

use frontend\assets\AppAssetAbout;
use yii\helpers\Html;

$this->title = 'Sobre';
$this->params['breadcrumbs'][] = $this->title;

AppAssetAbout::register($this);
?>
<div class="site-about">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase"><?= Html::encode($this->title) ?></h2>
            <h3 class="section-subheading text-muted">Vamos lá, meu nome é Danilo Antônio (caso você ainda não tenha percebido rsrs),
                graduando em Licenciatura em Computação pela Universidade de Brasília - UnB.<br>
                Fiz parte de ótimas equipes, das quais me orgulho bastante.<br>
                Tive colegas fantásticos que me ensinaram muito e graças a eles carrego comigo todo o conhecimento que tenho hoje.
            </h3>

        </div>
        <ul class="timeline">
            <li>
                <div class="timeline-image"><i class="fas fa-code fa-stack-1x fa-inverse fa-6x"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>2015</h4>
                        <h4 class="subheading">Primeiro estágio realizado!</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Realização do primeiro estágio e contato com desenvolvimento e manutenção de softwares. Uso da linguagem Java e banco de dados PostgreSQL.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><i class="fas fa-code fa-stack-1x fa-inverse fa-6x"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>2016-2018</h4>
                        <h4 class="subheading">Segundo estágio!</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Realização do segundo estágio em desenvolvimento, manutenção e documentação de softwares. Uso das linguagens PHP e Java, com banco de dados PostgreSQL e MySQL, Framework <a href="https://www.yiiframework.com/">Yii2.</a></p></div>
                </div>
            </li>
            <li>
                <div class="timeline-image"><i class="fas fa-building fa-stack-1x fa-inverse fa-6x"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Agosto 2018</h4>
                        <h4 class="subheading">Transição de estágio para serviços prestados sob Pessoa Jurídica</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Com a conclusão do estágio, obtive aproveitamento na empresa e passei a prestar serviços sob Pessoa Jurídica, ainda desenvolvendo nas mesmas linguagens e banco de dados mas em novos projetos, como por exemplo em API Rest.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image"><i class="fas fa-check fa-stack-1x fa-inverse fa-6x"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4>Atualmente</h4>
                    </div>
                    <div class="timeline-body"><p class="text-muted">Atualmente trabalho como desenvolvedor em uma empresa de marketing digital, <a href="https://www.ignicaodigital.com.br/">Ignição Digital</a>, e freelancer em projetos.</p></div>
                </div>
            </li>
            <li class="timeline-inverted">
                <div class="timeline-image">
                    <h4>Vamos fazer<br />Juntos<br />essa história!</h4>
                </div>
            </li>
        </ul>
    </div>
    <hr/>
</div>


