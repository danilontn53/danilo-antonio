<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Serviços';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="container">
        <div class="text-center">
            <h2 class="section-heading text-uppercase"><?= Html::encode($this->title) ?></h2>
            <h3 class="section-subheading text-muted">Desenvolvimento e manutenções diversas, seja em um novo projeto ou em um já existente.</h3>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <span class="fa-stack fa-4x"><i class="fas fa-circle fa-stack-2x text-primary"></i><i class="fas fa-code fa-stack-1x fa-inverse"></i></span>
                <h4 class="my-3">Desenvolvimento Back-End</h4>
                <p class="text-muted">Desenvolvimento Back-End em PHP com banco de dados em PostgreSQL e MySQL.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x"><i class="fas fa-circle fa-stack-2x text-primary"></i><i class="fas fa-at fa-stack-1x fa-inverse"></i></span>
                <h4 class="my-3">Web Sites</h4>
                <p class="text-muted">Desenvolvimento de web-sites com qualidade, fácil atualização e custo acessível.</p>
            </div>
            <div class="col-md-4">
                <span class="fa-stack fa-4x"><i class="fas fa-circle fa-stack-2x text-primary"></i><i class="fas fa-database fa-stack-1x fa-inverse"></i></span>
                <h4 class="my-3">API'S, manipulaçao de arquivos e dados</h4>
                <p class="text-muted">Desenvolvimento de API'S, Interface Client, manipulação de arquivos e dados para exibição em painéis.</p>
            </div>
        </div>
    </div>
    <hr/>
</div>
