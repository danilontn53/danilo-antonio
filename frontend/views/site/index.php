<?php

/* @var $this yii\web\View */

$this->title = 'Danilo Antônio';
?>
<div class="site-index">
    <section id="servicos">
        <?= $this->render('servicos') ?>
    </section>
    <section id="about">
        <?= $this->render('about') ?>
    </section>
    <section id="portfolio">
        <?= $this->render('portfolio') ?>
    </section>
</div>
