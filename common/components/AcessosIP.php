<?php
namespace common\components;

class AcessosIP
{
    public static function contabilizaAcesso() {
        //caminho arquivo txt contendo informações dos acessos por ip
        $url = str_replace("\\", "/", \Yii::$app->getBasePath() . '/web/acesso/arquivo_acessos.txt');
        //busca acessos arquivo txt
        $acessos = self::getAcessosTXT($url);
        //ip do acesso atual em sessão
        $ip_acesso = self::get_ip_address();
        //chave do acesso caso seja reincidente senão será falso (novo acesso ip diferente)
        $novo_acesso = array_search($ip_acesso, array_column($acessos, 'ip'));

        if ($novo_acesso !== false) {
            $acessos[$novo_acesso] = [
                'ip' => $ip_acesso,
                'num_acesso' => $acessos[$novo_acesso]['num_acesso'] + 1,
                'ult_acesso' => (string) date('d/m/Y H:i:s')
            ];
        } else {
            $acessos[] = [
                'ip' => $ip_acesso,
                'num_acesso' => (int) 1,
                'ult_acesso' => (string) date('d/m/Y H:i:s')
            ];
        }

        //atualiza arquivo txt
        self::updateAcessos($url, $acessos);
    }

    public static function getAcessosTXT($url) {
        $arquivo = fopen($url,"r ");

        $acessos = array();
        while(!feof($arquivo)){
            $line = fgets($arquivo);
            $inf = explode(', ', $line);
            if (count($inf) == 3) {
                $acessos[] = [
                    'ip' => (string)$inf[0],
                    'num_acesso' => (int)$inf[1],
                    'ult_acesso' => (string)preg_replace("/\r?\n/", "", $inf[2])
                ];
            }
        }
        fclose($arquivo);

        return $acessos;
    }

    public static function updateAcessos($url, $acessos) {
        //apaga arquivo
        unlink($url);

        //inicia um novo arquivo
        $open = fopen($url,"w");
        $quebra = chr(13).chr(10);//essa é a quebra de linha

        //ordena acessos por ip
        self::ordenaAcessosIP($acessos);

        foreach ($acessos as $key => $linha) {
            if (count($acessos) - 1 != $key){
                fwrite($open,$linha['ip'] . ", " . $linha['num_acesso'] . ", " . $linha['ult_acesso'] . $quebra);
            } else {
                fwrite($open,$linha['ip'] . ", " . $linha['num_acesso'] . ", " . $linha['ult_acesso']);
            }

        }
        fclose($open);
    }

    public static function ordenaAcessosIP(&$acessos) {
        $sortArray = array();
        foreach($acessos as $person){
            foreach($person as $key=>$value){
                if(!isset($sortArray[$key])){
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = $value;
            }
        }

        array_multisort($sortArray['ip'],SORT_ASC, $acessos);
    }

    public static function get_ip_address() {

        // Verifique se há IP compartilhado da Internet
        if (!empty($_SERVER['HTTP_CLIENT_IP']) && self::validar_ip($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        }

        // Verifique se há endereços IP passando por proxies
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {

            // Verifique se existem vários endereços IP em var
            if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
                $lista_ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

                foreach ($lista_ip as $ip) {
                    if (self::validar_ip($ip))
                        return $ip;
                }
            } else {

                if (self::validar_ip($_SERVER['HTTP_X_FORWARDED_FOR'])){
                    return $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
            }
        }

        if (!empty($_SERVER['HTTP_X_FORWARDED']) && self::validar_ip($_SERVER['HTTP_X_FORWARDED'])){
            return $_SERVER['HTTP_X_FORWARDED'];
        }

        if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && self::validar_ip($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])){
            return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        }

        if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && self::validar_ip($_SERVER['HTTP_FORWARDED_FOR'])){
            return $_SERVER['HTTP_FORWARDED_FOR'];
        }

        if (!empty($_SERVER['HTTP_FORWARDED']) && self::validar_ip($_SERVER['HTTP_FORWARDED'])){
            return $_SERVER['HTTP_FORWARDED'];
        }

        // Retornar endereço IP não confiável, pois tudo o mais falhou
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Garante que um endereço IP seja um endereço IP válido e não se enquadre
     * uma faixa de rede privada.
     */
    public static function validar_ip($ip) {

        if (strtolower($ip) === 'unknown') return false;

        // endereço de rede IPv4
        $ip = ip2long($ip);

        // Se o endereço IP estiver definido e não for equivalente a 255.255.255.255
        if ($ip !== false && $ip !== -1) {
            // Certifique-se de obter uma representação longa não assinada do endereço IP
            // devido a discrepâncias entre sistemas operacionais de 32 e 64 bits e
            // números assinados (ints padrão para logado no PHP)
            $ip = sprintf('%u', $ip);

            // Fazer verificação de faixa de rede privada
            if ($ip >= 0 && $ip <= 50331647) return false;

            if ($ip >= 167772160 && $ip <= 184549375) return false;

            if ($ip >= 2130706432 && $ip <= 2147483647) return false;

            if ($ip >= 2851995648 && $ip <= 2852061183) return false;

            if ($ip >= 2886729728 && $ip <= 2887778303) return false;

            if ($ip >= 3221225984 && $ip <= 3221226239) return false;

            if ($ip >= 3232235520 && $ip <= 3232301055) return false;

            if ($ip >= 4294967040) return false;
        }
        return true;
    }
}