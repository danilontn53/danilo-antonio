<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->name) ?></title>

    <!--  Rodapé  -->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/frontend/web/img/logo.png', ['title'=> Yii::$app->name]),
        'brandOptions' => ['class' => 'navbar-brand'],//options of the brand
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'id' => 'menu-transparent'
        ],
    ]);
    $menuItems = [
        ['label' => 'Início', 'url' => ['#inicio'], 'options' => ['class' => 'scroll-suave']],
        ['label' => 'Serviços', 'url' => ['#servicos'], 'options' => ['class' => 'scroll-suave']],
        ['label' => 'Sobre', 'url' => ['#about'], 'options' => ['class' => 'scroll-suave']],
        ['label' => 'Portfólio', 'url' => ['#portfolio'], 'options' => ['class' => 'scroll-suave']],
        ['label' => 'Contato', 'url' => ['#contato'], 'options' => ['class' => 'scroll-suave']],
    ];
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//    } else {
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/site/logout'], 'post')
//            . Html::submitButton(
//                'Logout (' . Yii::$app->user->identity->username . ')',
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
//    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <!-- Masthead-->
    <header class="masthead" id="inicio">
        <div class="container">
            <div class="masthead-subheading">Bem-vindo à Danilo Antônio!</div>
            <div class="masthead-heading text-uppercase">Você tranquilo, desenvolvendo mais e reverenciando o seu ambiente!</div>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#servicos">Conte-me mais</a>
        </div>
    </header>

    <div class="container">
        <?= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'links' => [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="third">
            <h4 id="contato">Contato</h4>
            <a>Danilo Antônio</a><br>
            <a><i class="far fa-envelope"></i> danilontn53@gmail.com</a><br>
            <a><i class="fas fa-map-marker-alt"></i> Brasília, Brasil </a>
        </div>
        <div class="second2">
            <a href="https://gitlab.com/danilontn53/" target="_blank"><i class="fab fa-gitlab fa-2x margin"></i></a>
            <a href="https://www.linkedin.com/in/danilo-ant%C3%B4nio-b73173106/" target="_blank"><i class="fab fa-linkedin fa-2x margin"></i></a>
            <a href="https://www.instagram.com/daniloantinio/" target="_blank"><i class="fab fa-instagram fa-2x margin" ></i></a>
            <a href="https://api.whatsapp.com/send?phone=5561999827465" target="_blank"><i class="fab fa-whatsapp fa-2x margin" ></i></a>
        </div>

        <div class="line"></div>

        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
